# rest-python-jwt

A Flask (JWT Extended) REST service which uses SQLAlchemy DB to demonstrate preliminary implementation of JWT in accessing endpoints.

## Preface

In REST services there are usually endpoints that needs restricted access; meaning that only authenticated users are able to invoke them. In this python REST service, users cannot simply access the `/secret` endpoint. In order to access `/secret` they must either register or login given the provided endpoints by which they will be provided with a JWT access & refresh token. They can afterward use these tokens to access the endpoint or refresh their access token.

Upon sending the JWT token to the `/secret` endpoint a simple `{"answer":42}` is returned, otherwise access will be restricted

## Available Endpoints

- `/registration` used for user registration
- `/login` used for user login
- `/logout/access` invalidate jwt access token
- `/logout/refresh` invalidate jwt refresh token
- `/token/refresh` used to refresh jwt token
- `/users` access list of users
- `/secret` indicates an endpoint with sensitive data

## File Structure

- `run.py`: the main executable file.
  - use `python3 run.py` to start service
  - creates a flask application and configures it with JWT and DB properties
  - wraps the flask application with flask restful API (configures endpoints)
  - initiates the database
  - starts the application
- `resources.py`: implementations for the user accessible endpoints
  - for each introduced endpoint, it provides a class and a method with proper jwt annotation
- `models.py`: it's designed to contain application models (e.g. User)
  - `UserModel`: indicates a user with `username` and `password` properties
    - provides the CRUD functionalities that a db table requires
  - `RevokedTokenModel`: indicates a model as well as functionalities for the revoked tokens

## JWT_SECRET_KEY

It's a property passed to configure flask application and it's the secret key needed for symmetric based signing algorithms, such as HS\*. If this is not set, the flask `SECRET_KEY` value is used instead.
This basically means that the REST services shall have access to such a key to function properly.

## @jwt_required

This is the annotation used to restrict the resource to those requests that contain the jwt token.
