from flask_restful import Resource, reqparse
from models import UserModel, RevokedTokenModel
import models
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    jwt_refresh_token_required,
    get_jwt_identity,
    get_raw_jwt
)
parser = reqparse.RequestParser()
parser.add_argument('username', help='field cannot be blank', required=True)
parser.add_argument('password', help='field cannot be blank', required=True)


class UserRegistration(Resource):
    def post(self):
        data = parser.parse_args()
        _username = data['username']
        _password = data['password']
        if UserModel.find_by_username(_username):
            return {'message': 'User {} already exists.'.format(_username)}

        new_user = UserModel(
            username=_username,
            _password=UserModel.generate_hash(_password)
        )
        try:
            new_user.save_to_db()
            access_token = create_access_token(identity=_username)
            refresh_token = create_refresh_token(identity=_username)
            return {
                'message': 'User {} was created'.format(_username),
                'access_token': access_token,
                'refresh_token': refresh_token
            }
        except:
            return {'message': 'Something went wrong'}, 500


class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        username = data['username']
        password = data['password']
        user = UserModel.find_by_username(username)

        if not user:
            return {'message': 'User {} not found'.format(username)}

        if UserModel.verify_hash(password, user.password):
            access_token = create_access_token(identity=username)
            refresh_token = create_refresh_token(identity=username)

            return {
                'message': 'Logged in as {}'.format(username),
                'access_token': access_token,
                'refresh_token': refresh_token
            }
        else:
            return {'message': 'Invalid credentials'}


class UserLogoutAccess(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Access token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Refresh token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}


class AllUsers(Resource):
    def get(self):
        # no need to jsonify the response.;
        return UserModel.return_all()

    def delete(self):
        return UserModel.delete_all()


class SecretResource(Resource):
    @jwt_required
    def get(self):
        return {
            # no need to jsonify the response.;
            'answer': 42
        }
